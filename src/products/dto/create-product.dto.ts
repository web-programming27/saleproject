import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(4)
  name: string;

  // @IsPositive()
  @IsNotEmpty()
  size: number;

  @IsNotEmpty()
  size_unit: string;

  // @IsPositive()
  @IsNotEmpty()
  price: number;

  image = 'no_image.jpg';
  @IsNotEmpty()
  catagoryId: number;
}
