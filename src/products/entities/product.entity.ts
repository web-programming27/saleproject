import { Catagory } from 'src/catagorys/entities/catagory.entity';
import { Reciept } from 'src/reciepts/entities/reciept.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  size: number;

  @Column()
  size_unit: string;

  @Column({
    type: 'float',
  })
  price: number;

  @Column({
    length: '128',
    default: 'no_image.jpg',
  })
  image: string;

  @ManyToOne(() => Catagory, (catagory) => catagory.products)
  catagory: Catagory;

  @OneToMany(() => Reciept, (reciept) => reciept.recieptDetails)
  reciept: Reciept;
  @Column()
  catagoryId: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
