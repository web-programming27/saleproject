import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { InjectRepository } from '@nestjs/typeorm/dist/common/typeorm.decorators';
import { Repository } from 'typeorm/repository/Repository';
import { Catagory } from 'src/catagorys/entities/catagory.entity';
import { Like } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Catagory)
    private catagorysRepository: Repository<Catagory>,
  ) {}
  async create(createProductDto: CreateProductDto) {
    const catagory = await this.catagorysRepository.findOne({
      where: {
        id: createProductDto.catagoryId,
      },
    });
    console.log(catagory);
    const product = new Product();
    product.name = createProductDto.name;
    product.size = createProductDto.size;
    product.size_unit = createProductDto.size_unit;
    product.price = createProductDto.price;
    product.image = createProductDto.image;
    product.catagory = catagory;
    await this.productsRepository.save(product);
    console.log(product);
    return this.productsRepository.findOne({
      where: { id: product.id },
      relations: ['catagory'],
    });
  }

  findByCatagory(id: number) {
    return this.productsRepository.find({ where: { catagoryId: id } });
  }

  async findAll(query): Promise<Paginate> {
    const page = query.page || 1;
    const take = query.take || 10;
    const skip = (page - 1) * take;
    const keyword = query.keyword || '';
    const orderBy = query.orderBy || 'name';
    const order = query.order || 'ASC';
    const currentPage = page;
    const [result, total] = await this.productsRepository.findAndCount({
      relations: ['catagory'],
      where: { name: Like(`%${keyword}%`) },
      order: { [orderBy]: order },
      take: take,
      skip: skip,
    });
    const lastPage = Math.ceil(total / take);
    return {
      data: result,
      count: total,
      currentPage: currentPage,
      lastPage: lastPage,
    };
  }

  async findOne(id: number) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
      relations: ['catagory'],
    });
    if (!product) {
      throw new NotFoundException();
    }
    return product;
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
    });
    if (!product) {
      throw new NotFoundException();
    }
    const updatedProduct = { ...product, ...updateProductDto };
    return this.productsRepository.save(updatedProduct);
  }

  async remove(id: number) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
    });
    if (!product) {
      throw new NotFoundException();
    }
    return this.productsRepository.softRemove(product);
  }
}
