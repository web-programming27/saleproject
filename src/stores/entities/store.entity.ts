import { Reciept } from 'src/reciepts/entities/reciept.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Store {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  address_detail: string;
  @Column()
  address_sub_district: string;
  @Column()
  address_district: string;
  @Column()
  address_province: string;

  @Column({ unique: true })
  tel: string;
  @CreateDateColumn()
  createdDate: Date;
  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => Reciept, (reciepts) => reciepts.store)
  reciepts: Reciept[];
}
