import { Module } from '@nestjs/common';
import { StoresService } from './stores.service';
import { StoresController } from './stores.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reciept } from 'src/reciepts/entities/reciept.entity';
import { Store } from './entities/store.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Reciept, Store])],
  controllers: [StoresController],
  providers: [StoresService],
})
export class StoresModule {}
