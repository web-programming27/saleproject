import { IsNotEmpty, MinLength } from 'class-validator';

export class CreateStoreDto {
  @IsNotEmpty()
  @MinLength(4)
  name: string;
  @IsNotEmpty()
  address_detail: string;
  @IsNotEmpty()
  address_sub_district: string;
  @IsNotEmpty()
  address_district: string;
  @IsNotEmpty()
  address_province: string;

  @IsNotEmpty()
  @MinLength(10)
  tel: string;
}
