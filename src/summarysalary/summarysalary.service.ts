import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateSummarysalaryDto } from './dto/create-summarysalary.dto';
import { UpdateSummarysalaryDto } from './dto/update-summarysalary.dto';
import { Summarysalary } from './entities/summarysalary.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class SummarysalaryService {
  constructor(
    @InjectRepository(Summarysalary)
    private summarysalaryRepository: Repository<Summarysalary>,
    @InjectRepository(Checkinout)
    private checkinoutsRepository: Repository<Checkinout>,
    @InjectRepository(User)
    private employeesRepository: Repository<User>,
  ) {}
  // create(createSummarysalaryDto: CreateSummarysalaryDto) {
  //   return this.summarysalaryRepository.save(createSummarysalaryDto);
  // }

  findAll() {
    return this.summarysalaryRepository.find({
      relations: ['checkinout', 'employee'],
    });
  }

  async findOne(id: number) {
    const summarysalary = await this.summarysalaryRepository.findOne({
      where: { id: id },
    });
    if (!summarysalary) {
      throw new NotFoundException();
    }
    return summarysalary;
  }

  async update(id: number, updateSummarysalaryDto: UpdateSummarysalaryDto) {
    const summarysalary = await this.summarysalaryRepository.findOneBy({
      id: id,
    });
    if (!summarysalary) {
      throw new NotFoundException();
    }
    const updatedSummarysalary = {
      ...summarysalary,
      ...updateSummarysalaryDto,
    };
    return this.summarysalaryRepository.save(updatedSummarysalary);
  }

  async remove(id: number) {
    const summarysalary = await this.summarysalaryRepository.findOneBy({
      id: id,
    });
    if (!summarysalary) {
      throw new NotFoundException();
    }

    return this.summarysalaryRepository.softRemove(summarysalary);
  }
}
