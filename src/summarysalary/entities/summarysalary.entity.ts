import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class Summarysalary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ss_date: Date;

  @Column({ type: 'float', default: 0 })
  ss_work_hour: number;

  @Column({ type: 'float', default: 0 })
  ss_salary: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => Checkinout, (checkinout) => checkinout.summarysalary)
  checkinout: Checkinout[];

  @ManyToOne(() => User, (employee) => employee.summarysalary)
  employee: User;
}
