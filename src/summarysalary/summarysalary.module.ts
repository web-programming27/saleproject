import { Module } from '@nestjs/common';
import { SummarysalaryService } from './summarysalary.service';
import { SummarysalaryController } from './summarysalary.controller';
import { Summarysalary } from './entities/summarysalary.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Summarysalary, Checkinout, User])],
  controllers: [SummarysalaryController],
  providers: [SummarysalaryService],
})
export class SummarysalaryModule {}
