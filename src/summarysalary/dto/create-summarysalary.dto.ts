import { IsNotEmpty } from 'class-validator';

export class CreateSummarysalaryDto {
  @IsNotEmpty()
  ss_work_hour: number;
  @IsNotEmpty()
  ss_salary: number;
  summarysalaryId: number;
}
