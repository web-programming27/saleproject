import { IsNotEmpty } from 'class-validator';

export class CreateCheckinoutDto {
  time_out: Date;
  @IsNotEmpty()
  employeeId: number;
}
