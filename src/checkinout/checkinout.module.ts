import { Module } from '@nestjs/common';
import { CheckinoutService } from './checkinout.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkinout } from './entities/checkinout.entity';
import { Summarysalary } from 'src/summarysalary/entities/summarysalary.entity';
import { CheckinoutController } from './checkinout.controller';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Checkinout, User, Summarysalary])],
  controllers: [CheckinoutController],
  providers: [CheckinoutService],
})
export class CheckinoutModule {}
