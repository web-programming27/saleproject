import { Summarysalary } from 'src/summarysalary/entities/summarysalary.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  id: number;
  @CreateDateColumn({ type: 'datetime' })
  time_in: Date;
  @Column({ type: 'datetime', default: null })
  time_out: Date;
  @Column({ default: 0, type: 'float' })
  total_hour: number;
  @Column()
  employeeId: number;
  @Column({ default: null })
  summarysalaryId: number;
  @DeleteDateColumn()
  deletedDate: Date;
  @ManyToOne(() => Summarysalary, (summarysalary) => summarysalary.checkinout)
  summarysalary: Summarysalary;
  @ManyToOne(() => User, (employee) => employee.checkinout)
  employee: User;
}
