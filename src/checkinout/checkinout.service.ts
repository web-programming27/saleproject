import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckinoutDto } from './dto/create-checkinout.dto';
import { UpdateCheckinoutDto } from './dto/update-checkinout.dto';
import { Checkinout } from './entities/checkinout.entity';
import { Summarysalary } from 'src/summarysalary/entities/summarysalary.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class CheckinoutService {
  constructor(
    @InjectRepository(Checkinout)
    private checkinoutsRepository: Repository<Checkinout>,
    @InjectRepository(User)
    private employeesRepository: Repository<User>,
    @InjectRepository(Summarysalary)
    private summarysalarysRepository: Repository<Summarysalary>,
  ) {}
  async create(createCheckinoutDto: CreateCheckinoutDto) {
    const employee = await this.employeesRepository.findOne({
      where: {
        id: createCheckinoutDto.employeeId,
      },
    });
    console.log(employee);
    const checkinout = new Checkinout();
    checkinout.time_in = new Date();
    console.log('timein', checkinout.time_in);
    checkinout.time_out = null;
    checkinout.employee = employee;
    await this.checkinoutsRepository.save(checkinout);
    try {
      const exitingss = await this.summarysalarysRepository.findOne({
        where: { checkinout: { employee: { id: checkinout.employee.id } } },
      });
      console.log(exitingss);
      if (!exitingss) {
        const summarySalary = new Summarysalary();
        checkinout.summarysalaryId = summarySalary.id;
        summarySalary.ss_date = new Date();
        summarySalary.ss_work_hour = checkinout.total_hour;
        summarySalary.ss_salary =
          checkinout.employee.hourlywage * checkinout.total_hour;
        summarySalary.employee = checkinout.employee;
        const newSummarySalary = await this.summarysalarysRepository.save(
          summarySalary,
        );
        console.log('summarySalary:', summarySalary);
        console.log('summarySalary:', summarySalary);
        checkinout.summarysalary = newSummarySalary;
      } else {
        exitingss.ss_work_hour += checkinout.total_hour;
        exitingss.ss_salary +=
          checkinout.employee.hourlywage * checkinout.total_hour;
        const updatedSummarySalary = await this.summarysalarysRepository.save(
          exitingss,
        );
        checkinout.summarysalary = updatedSummarySalary;
      }
      console.log(checkinout);

      return await this.checkinoutsRepository.save(checkinout);
    } catch (e) {
      console.log(e);
    }
  }

  findAll() {
    return this.checkinoutsRepository.find({
      relations: ['employee', 'summarysalary'],
    });
  }

  async findOne(id: number) {
    const checkinout = await this.checkinoutsRepository.findOne({
      where: { id: id },
      relations: ['employee', 'summarysalary'],
    });
    if (!checkinout) {
      throw new NotFoundException();
    }
    return checkinout;
  }

  async update(id: number) {
    const checkinout = await this.checkinoutsRepository.findOne({
      where: { id: id },
    });
    console.log(checkinout);
    console.log('id summsa', checkinout.summarysalaryId);
    if (!checkinout || !checkinout.time_in) {
      throw new NotFoundException();
    }

    if (!checkinout.time_out) {
      checkinout.time_out = new Date();
    }
    //cal work hour
    if (checkinout.time_in && checkinout.time_out) {
      const timeInMs = checkinout.time_in.getTime();
      const timeOutMs = checkinout.time_out.getTime();
      const workHourMs = timeOutMs - timeInMs;
      const workHour = Math.abs(workHourMs / (1000 * 60 * 60));
      checkinout.total_hour = workHour;
      console.log('totolhour in check in out', checkinout.total_hour);
      await this.checkinoutsRepository.save(checkinout);
      //update summary
      try {
        const ss = await this.summarysalarysRepository.findOne({
          where: { id: checkinout.summarysalaryId },
        });
        const emp = await this.employeesRepository.findOne({
          where: { id: checkinout.employeeId },
        });
        console.log('epm salary', emp.hourlywage);
        console.log('หาตัวนี้', ss);
        console.log('total hour', checkinout.total_hour);
        ss.ss_work_hour = ss.ss_work_hour + checkinout.total_hour;
        console.log('work hour:', ss.ss_work_hour);
        ss.ss_salary = emp.hourlywage * ss.ss_work_hour + ss.ss_salary;
        return await this.summarysalarysRepository.save(ss);
      } catch (e) {
        console.log(e);
      }
    }
    return await this.checkinoutsRepository.save(checkinout);
  }

  async remove(id: number) {
    const checkinout = await this.checkinoutsRepository.findOneBy({
      id: id,
    });
    if (!checkinout) {
      throw new NotFoundException();
    }
    return this.checkinoutsRepository.softRemove(checkinout);
  }
}
