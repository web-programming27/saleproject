import { Bill } from 'src/bills/entities/bill.entity';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { Checkmaterial } from 'src/checkmaterials/entities/checkmaterial.entity';
import { Reciept } from 'src/reciepts/entities/reciept.entity';
import { Summarysalary } from 'src/summarysalary/entities/summarysalary.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;
  @Column({
    length: '64',
  })
  login: string;
  @Column({
    length: '128',
  })
  password: string;

  @Column()
  address: string;
  @Column({ unique: true })
  tel: string;

  @Column()
  email: string;

  @Column()
  position: string;
  @Column({ type: 'float' })
  hourlywage: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => Reciept, (reciepts) => reciepts.employee)
  reciepts: Reciept[];

  @OneToMany(() => Checkmaterial, (checkmaterials) => checkmaterials.employee)
  checkmaterials: Checkmaterial[];

  @OneToMany(() => Bill, (bill) => bill.employee)
  bill: Bill[];

  @OneToMany(() => Checkinout, (checkinout) => checkinout.employee)
  checkinout: Checkinout[];

  @OneToMany(() => Summarysalary, (summarysalary) => summarysalary.employee)
  summarysalary: Summarysalary[];
}
