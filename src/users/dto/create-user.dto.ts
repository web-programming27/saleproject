import { IsNotEmpty, Length, IsEmail, Matches } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  @IsEmail()
  login: string;

  @IsNotEmpty()
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
  )
  password: string;
  @IsNotEmpty()
  address: string;
  @IsNotEmpty()
  tel: string;
  @IsNotEmpty()
  email: string;
  @IsNotEmpty()
  position: string;
  @IsNotEmpty()
  hourlywage: number;
}
