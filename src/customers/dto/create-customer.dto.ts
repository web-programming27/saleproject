import { IsNotEmpty, MinLength, MaxLength } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty({ message: 'ชื่อห้ามว่าง' })
  @MinLength(4, { message: 'ชื่อห้ามต่ำกว่า 4 ตัวอักษร' })
  @MaxLength(16, { message: 'ชื่อห้ามมากกว่า 16 ตัวอักษร' })
  name: string;

  @IsNotEmpty({ message: 'เบอร์ห้ามว่าง' })
  @MinLength(10, { message: 'เบอร์ห้ามต่ำกว่า 10 ตัว' })
  tel: string;

  @IsNotEmpty({ message: 'คะแนนห้ามว่าง' })
  point: number;
}
