import { Injectable, Logger } from '@nestjs/common';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class ReportsService {
  private readonly logger = new Logger();
  constructor(@InjectDataSource() private dataSource: DataSource) {}
  @Cron(CronExpression.EVERY_DAY_AT_1AM)
  async tryToRun() {
    console.log('update fact');
    return await this.dataSource.query(
      'INSERT INTO FactDw(Fact_quantity_sold, Fact_total_sales, Fact_discount, Time_id, User_id, Store_id, Customer_id, Product_id) SELECT SUM(recieptdetail.amount), SUM(recieptdetail.total), SUM(reciept.discount), Time_id, User_id, Store_id, Customer_id, Product_id FROM recieptdetail INNER JOIN reciept ON recieptdetail.recieptId = reciept.id INNER JOIN time_dw ON CAST(reciept.date AS DATETIME) = time_dw.time_original INNER JOIN user_dw ON reciept.employeeId = user_dw.user_id INNER JOIN store_dw ON reciept.storeId = store_dw.store_id LEFT JOIN customer_dw ON reciept.customerId= customer_dw.customer_id INNER JOIN product_dw ON recieptdetail.productId = product_dw.product_id GROUP BY time_dw.time_id, reciept.date, reciept.employeeId, reciept.storeId, reciept.customerId, recieptdetail.productId;',
    );
  }
  handleCron() {
    this.logger.debug('Called every 2 HOUR');
  }
  getProduct() {
    return this.dataSource.query('SELECT * FROM product');
  }

  getCustomer() {
    return this.dataSource.query('SELECT * FROM customer');
  }

  getProductBySearchText(searchText: any) {
    return this.dataSource.query('SELECT * FROM product WHERE name LIKE ?', [
      `%${searchText}%`,
    ]);
  }

  async getBestOrderChart() {
    const query =
      'SELECT recieptdetail.name, SUM(recieptdetail.amount) AS bestseller FROM recieptdetail GROUP BY recieptdetail.name, MONTH(recieptdetail.createdDate) ORDER BY bestseller DESC LIMIT 5;';
    const result = await this.dataSource.query(query);
    return result;
  }

  async getWorstOrderChart() {
    const query =
      'SELECT recieptdetail.name, SUM(recieptdetail.amount) AS worstseller FROM recieptdetail GROUP BY recieptdetail.name, MONTH(recieptdetail.createdDate) ORDER BY worstseller ASC LIMIT 5;';
    const result = await this.dataSource.query(query);
    return result;
  }

  async getCusByMonthChart() {
    const query =
      'SELECT COUNT(customer_dw.customer_id) AS member, MONTHNAME(customer_dw.customer_start_date) AS month_name FROM customer_dw GROUP BY month_name;';
    const result = await this.dataSource.query(query);
    return result;
  }

  async getSaleChart() {
    const query =
      'SELECT time_of_day, SUM(sales) AS total_sales FROM (SELECT SUM(reciept.total) AS sales, CASE WHEN(HOUR(reciept.date) BETWEEN 6 AND 8) THEN "Dawn" WHEN(HOUR(reciept.date) BETWEEN 9 AND 11) THEN "Morning" WHEN(HOUR(reciept.date) BETWEEN 11 AND 12) THEN "Noon" WHEN(HOUR(reciept.date) BETWEEN 13 AND 15) THEN "Afternoon" WHEN(HOUR(reciept.date) BETWEEN 16 AND 18) THEN "Early Evening" ELSE "Other" END AS time_of_day FROM reciept GROUP BY reciept.date) AS t GROUP BY time_of_day;';
    const result = await this.dataSource.query(query);
    return result;
  }

  async getCustomerBySearchTel(query: any) {
    console.log(query.searchTel);
    const cuslist = await this.dataSource.query(
      `CALL getcustomerTel('${query.searchTel}')`,
    );
    return cuslist[0];
  }

  getCustomerByPoint() {
    return this.dataSource.query('SELECT * FROM customer_info_point');
  }

  getEmployeeBySale() {
    return this.dataSource.query('CALL `getTopEmp`();');
  }

  getProductByCatagory(searchCatagory: any) {
    return this.dataSource.query('CALL getProduct2(?)', [searchCatagory]);
  }

  getViewCustomer() {
    return this.dataSource.query('SELECT * FROM customer_info_point');
  }
  sumOrder() {
    return this.dataSource.query('SELECT * FROM reciept_amount');
  }

  sumSale() {
    return this.dataSource.query(
      'SELECT SUM(Fact_total_sales_agg) AS sumsale FROM FactAgg GROUP BY Month_id;',
    );
  }

  create(createReportDto: CreateReportDto) {
    return 'This action adds a new report';
  }

  findAll() {
    return `This action returns all reports`;
  }

  findOne(id: number) {
    return `This action returns a #${id} report`;
  }

  update(id: number, updateReportDto: UpdateReportDto) {
    return `This action updates a #${id} report`;
  }

  remove(id: number) {
    return `This action removes a #${id} report`;
  }
}
