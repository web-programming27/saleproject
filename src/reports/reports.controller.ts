import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ReportsService } from './reports.service';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { query } from 'express';
import { search } from 'superagent';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}
  @Get('/product')
  getProduct(
    @Query()
    query: {
      lowPrice?: number;
      upperPrice?: number;
      searchText?: string;
    },
  ) {
    console.log(query);
    if (query.searchText) {
      return this.reportsService.getProductBySearchText(query.searchText);
    }
    return this.reportsService.getProduct();
  }

  @Get('/customer')
  getCustomer(
    @Query()
    query: {
      searchTel?: string;
    },
  ) {
    console.log(query);

    return this.reportsService.getCustomerBySearchTel(query);
  }

  @Get('/customer/bypoint')
  getCustomerByPoint() {
    return this.reportsService.getCustomerByPoint();
  }
  @Get('/view/customer')
  getViewCustomer() {
    return this.reportsService.getViewCustomer();
  }
  @Get('/sumorder')
  sumOrder() {
    return this.reportsService.sumOrder();
  }

  @Get('/sumsale')
  sumSale() {
    return this.reportsService.sumSale();
  }

  @Get('/product/bycatagory')
  getProductByCatagory(
    @Query()
    query: {
      searchCatagory?: string;
    },
  ) {
    {
      console.log(query);
      if (query.searchCatagory) {
        return this.reportsService.getProductByCatagory(query.searchCatagory);
      }
    }
  }
  //สินค้าขายดี5 อันดับ
  @Get('/recieptdetail/bestseller/chart')
  getBestOrderChart() {
    return this.reportsService.getBestOrderChart();
  }
  //สินค้าขายไม่ออก 5 อันดับ
  @Get('/recieptdetail/worstseller/chart')
  getWorstOrderChart() {
    return this.reportsService.getWorstOrderChart();
  }
  //ดู customer ตามเดือน สมัครใหม่
  @Get('/customer/bymonth/chart')
  getCusByMonthChart() {
    return this.reportsService.getCusByMonthChart();
  }
  //ดูยอดขายรวมตามเวลา
  @Get('/reciept/sale')
  getSaleChart() {
    return this.reportsService.getSaleChart();
  }
  //employee 5 อันดับ
  @Get('/view/employee/sale')
  getEmployeeBySale() {
    return this.reportsService.getEmployeeBySale();
  }
}
