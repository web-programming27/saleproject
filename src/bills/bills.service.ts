import { NotFoundException } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';
import { User } from 'src/users/entities/user.entity';
import { Material } from 'src/materials/entities/material.entity';
import { Billdetail } from './entities/billdetail';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bill)
    private billRepository: Repository<Bill>,
    @InjectRepository(User)
    private employeesRepository: Repository<User>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
    @InjectRepository(Billdetail)
    private billdetailRepository: Repository<Billdetail>,
  ) {}

  async create(createBillDto: CreateBillDto) {
    const employee = await this.employeesRepository.findOne({
      where: {
        id: createBillDto.employeeId,
      },
    });
    console.log(employee);
    const bill = new Bill();
    bill.name = createBillDto.name;
    bill.amount = 0;
    bill.total = 0;
    bill.buy = createBillDto.buy;
    bill.change = 0;
    bill.employee = employee;
    await this.billRepository.save(bill);

    for (const bd of createBillDto.billDetail) {
      const billdetail = new Billdetail();
      billdetail.amount = bd.amount;
      billdetail.material = await this.materialRepository.findOneBy({
        id: bd.materialId,
      });
      console.log(billdetail);
      billdetail.name = billdetail.material.name;
      billdetail.price = billdetail.material.price_per_unit;
      billdetail.total = billdetail.price * billdetail.amount;
      billdetail.bill = bill;

      await this.billdetailRepository.save(billdetail);
      bill.amount = bill.amount + billdetail.amount;
      bill.total = bill.total + billdetail.total;
      bill.change = bill.buy - bill.total;

      const updtadematerial = billdetail.material;
      updtadematerial.quantity = updtadematerial.quantity + billdetail.amount;
      await this.materialRepository.save(updtadematerial);
    }
    await this.billRepository.save(bill);
    return this.billRepository.findOne({
      where: { id: bill.id },
      relations: ['billDetails'],
    });
  }

  findAll() {
    return this.billRepository.find({ relations: ['employee'] });
  }

  async findOne(id: number) {
    const bill = await this.billRepository.findOne({
      where: { id: id },
      relations: ['employee'],
    });
    if (!bill) {
      throw new NotFoundException();
    }
    return bill;
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    const bill = await this.billRepository.findOneBy({
      id: id,
    });
    if (!bill) {
      throw new NotFoundException();
    }
    const updateBill = { ...bill, ...updateBillDto };
    return this.billRepository.save(updateBill);
  }

  async remove(id: number) {
    const bill = await this.billRepository.findOneBy({
      id: id,
    });
    if (!bill) {
      throw new NotFoundException();
    }
    return this.billRepository.softRemove(bill);
  }
}
