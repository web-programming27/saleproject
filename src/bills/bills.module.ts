import { Module } from '@nestjs/common';
import { BillsService } from './bills.service';
import { BillsController } from './bills.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bill } from './entities/bill.entity';
import { Material } from 'src/materials/entities/material.entity';
import { User } from 'src/users/entities/user.entity';
import { Billdetail } from './entities/billdetail';

@Module({
  imports: [TypeOrmModule.forFeature([Bill, Material, User, Billdetail])],
  controllers: [BillsController],
  providers: [BillsService],
})
export class BillsModule {}
