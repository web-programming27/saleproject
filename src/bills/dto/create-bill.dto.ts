import { IsNotEmpty } from 'class-validator';

class CreateBillDetailDto {
  @IsNotEmpty()
  materialId: number;
  @IsNotEmpty()
  amount: number;
}
export class CreateBillDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  buy: number;
  billDetail: CreateBillDetailDto[];
  @IsNotEmpty()
  employeeId: number;
}
