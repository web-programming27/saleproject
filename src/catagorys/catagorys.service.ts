import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCatagoryDto } from './dto/create-catagory.dto';
import { UpdateCatagoryDto } from './dto/update-catagory.dto';
import { Catagory } from './entities/catagory.entity';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class CatagorysService {
  constructor(
    @InjectRepository(Catagory)
    private catagorysRepository: Repository<Catagory>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}
  create(createCatagoryDto: CreateCatagoryDto) {
    return this.catagorysRepository.save(createCatagoryDto);
  }

  findAll() {
    return this.catagorysRepository.find();
  }

  async findOne(id: number) {
    const catagory = await this.catagorysRepository.findOne({
      where: { id: id },
      relations: ['product'],
    });
    if (!catagory) {
      throw new NotFoundException();
    }
    return catagory;
  }

  async update(id: number, updateCatagoryDto: UpdateCatagoryDto) {
    const catagory = await this.catagorysRepository.findOneBy({ id: id });
    if (!catagory) {
      throw new NotFoundException();
    }
    const updatedCatagory = { ...catagory, ...updateCatagoryDto };
    return this.catagorysRepository.save(updatedCatagory);
  }

  async remove(id: number) {
    const catagory = await this.catagorysRepository.findOneBy({ id: id });
    if (!catagory) {
      throw new NotFoundException();
    }
    return this.catagorysRepository.softRemove(catagory);
  }
}
