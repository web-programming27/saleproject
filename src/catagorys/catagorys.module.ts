import { Module } from '@nestjs/common';
import { CatagorysService } from './catagorys.service';
import { CatagorysController } from './catagorys.controller';
import { Catagory } from './entities/catagory.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from 'src/products/entities/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Catagory, Product])],
  controllers: [CatagorysController],
  providers: [CatagorysService],
})
export class CatagorysModule {}
