import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersModule } from './customers/customers.module';
import { CatagorysModule } from './catagorys/catagorys.module';
import { DataSource } from 'typeorm';
import { Customer } from './customers/entities/customer.entity';
import { Catagory } from './catagorys/entities/catagory.entity';
import { ProductsModule } from './products/products.module';
import { Product } from './products/entities/product.entity';
import { Reciept } from './reciepts/entities/reciept.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Recieptdetail } from './reciepts/entities/recieptdetail';
import { Store } from './stores/entities/store.entity';
import { StoresModule } from './stores/stores.module';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { ReportsModule } from './reports/reports.module';
import { CheckmaterialsModule } from './checkmaterials/checkmaterials.module';
import { MaterialsModule } from './materials/materials.module';
import { BillsModule } from './bills/bills.module';
import { Checkmaterial } from './checkmaterials/entities/checkmaterial.entity';
import { Material } from './materials/entities/material.entity';
import { Bill } from './bills/entities/bill.entity';
import { SummarysalaryModule } from './summarysalary/summarysalary.module';
import { CheckinoutModule } from './checkinout/checkinout.module';
import { Summarysalary } from './summarysalary/entities/summarysalary.entity';
import { Checkinout } from './checkinout/entities/checkinout.entity';
import { RecieptsModule } from './reciepts/reciepts.module';
import { Billdetail } from './bills/entities/billdetail';
import { Checkmaterialdetail } from './checkmaterials/entities/checkmaterialdetail';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'sale-database.cnqqama4e1dy.ap-southeast-1.rds.amazonaws.com',
      port: 3306,
      username: 'sale',
      password: 'sale-password',
      database: 'sale_database',
      entities: [
        Store,
        Customer,
        Catagory,
        Product,
        Reciept,
        Recieptdetail,
        User,
        Checkmaterial,
        Checkmaterialdetail,
        Material,
        Billdetail,
        Bill,
        Summarysalary,
        Checkinout,
      ],
      synchronize: true,
      autoLoadEntities: true,
    }),
    StoresModule,
    CustomersModule,
    CatagorysModule,
    ProductsModule,
    RecieptsModule,
    UsersModule,
    AuthModule,
    ReportsModule,
    CheckmaterialsModule,
    MaterialsModule,
    BillsModule,
    SummarysalaryModule,
    CheckinoutModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
// {
//   type: 'mysql',
//   host: 'localhost',
//   port: 3306,
//   username: 'coffee-project',
//   password: 'Pass@1234',
//   database: 'coffee_project',
//   entities: [
//     Store,
//     Employee,
//     Customer,
//     Catagory,
//     Product,
//     Reciept,
//     Recieptdetail,
//   ],
//   synchronize: true,
// },

// {
//   type: 'sqlite',
//   database: 'db.sqlite',
//   synchronize: true,
//   migrations: [],
//   entities: [
//     Store,
//     Employee,
//     Customer,
//     Catagory,
//     Product,
//     Reciept,
//     Recieptdetail,
//     User,
//     Promotion,
//     Checkmaterial,
//     Checkmaterialdetail,
//     Material,
//     Billdetail,
//     Bill,
//   ],
// },
// {
//   type: 'mysql',
//   host: 'db4free.net',
//   port: 3306,
//   username: 'database_proweb',
//   password: 'Pass@1234',
//   database: 'database_proweb',
//   entities: [
//     Store,
//     Customer,
//     Catagory,
//     Product,
//     Reciept,
//     Recieptdetail,
//     User,
//     Checkmaterial,
//     Checkmaterialdetail,
//     Material,
//     Billdetail,
//     Bill,
//     Summarysalary,
//     Checkinout,
//   ],
//   synchronize: true,
// }
