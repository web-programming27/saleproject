import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCheckmaterialDto } from './dto/create-checkmaterial.dto';
import { UpdateCheckmaterialDto } from './dto/update-checkmaterial.dto';
import { Checkmaterial } from './entities/checkmaterial.entity';
import { User } from 'src/users/entities/user.entity';
import { Checkmaterialdetail } from './entities/checkmaterialdetail';
import { Material } from 'src/materials/entities/material.entity';

@Injectable()
export class CheckmaterialsService {
  constructor(
    @InjectRepository(Checkmaterial)
    private checkmaterialsRepository: Repository<Checkmaterial>,
    @InjectRepository(User)
    private employeesRepository: Repository<User>,
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
    @InjectRepository(Checkmaterialdetail)
    private checkmaterialdetailsRepository: Repository<Checkmaterialdetail>,
  ) {}

  async create(createCheckmaterialDto: CreateCheckmaterialDto) {
    const employee = await this.employeesRepository.findOne({
      where: {
        id: createCheckmaterialDto.employeeId,
      },
    });
    console.log(employee);
    const checkmaterial = new Checkmaterial();
    checkmaterial.employee = employee;
    await this.checkmaterialsRepository.save(checkmaterial);
    for (const cm of createCheckmaterialDto.checkmaterialDetails) {
      const checkmaterialDetail = new Checkmaterialdetail();
      checkmaterialDetail.material = await this.materialsRepository.findOneBy({
        id: cm.materialId,
      });
      console.log(checkmaterialDetail);
      checkmaterialDetail.name = checkmaterialDetail.material.name;
      checkmaterialDetail.qty_last = checkmaterialDetail.material.quantity;
      checkmaterialDetail.qty_remain = cm.qty_remain;
      checkmaterialDetail.qty_expire = cm.qty_expire;
      checkmaterialDetail.checkmaterial = checkmaterial;
      await this.checkmaterialdetailsRepository.save(checkmaterialDetail);

      const updatematerial = checkmaterialDetail.material;
      updatematerial.quantity = checkmaterialDetail.qty_remain;
      await this.materialsRepository.save(updatematerial);
    }
    await this.checkmaterialsRepository.save(checkmaterial);
    return await this.checkmaterialsRepository.findOne({
      where: { id: checkmaterial.id },
      relations: ['checkmaterialdetail'],
    });
  }

  findAll() {
    return this.checkmaterialsRepository.find({ relations: ['employee'] });
  }

  async findOne(id: number) {
    const checkmaterial = await this.checkmaterialsRepository.findOne({
      where: { id: id },
      relations: ['employee'],
    });
    if (!checkmaterial) {
      throw new NotFoundException();
    }
    return checkmaterial;
  }

  async update(id: number, updateCheckmaterialDto: UpdateCheckmaterialDto) {
    const checkmaterial = await this.checkmaterialsRepository.findOneBy({
      id: id,
    });
    if (!checkmaterial) {
      throw new NotFoundException();
    }
    const updateCheckmaterial = { ...checkmaterial, ...updateCheckmaterialDto };
    return this.checkmaterialsRepository.save(updateCheckmaterial);
  }

  async remove(id: number) {
    const checkmaterial = await this.checkmaterialsRepository.findOneBy({
      id: id,
    });
    if (!checkmaterial) {
      throw new NotFoundException();
    }
    return this.checkmaterialsRepository.softRemove(checkmaterial);
  }
}
