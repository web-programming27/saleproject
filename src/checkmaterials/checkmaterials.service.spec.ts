import { Test, TestingModule } from '@nestjs/testing';
import { CheckmaterialsService } from './checkmaterials.service';

describe('CheckmaterialsService', () => {
  let service: CheckmaterialsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckmaterialsService],
    }).compile();

    service = module.get<CheckmaterialsService>(CheckmaterialsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
