import { Material } from 'src/materials/entities/material.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Checkmaterial } from './checkmaterial.entity';

@Entity()
export class Checkmaterialdetail {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  qty_last: number;
  @Column({ default: 0 })
  qty_remain: number;
  @Column({ default: 0 })
  qty_expire: number;

  @ManyToOne(
    () => Checkmaterial,
    (checkmaterial) => checkmaterial.checkmaterialdetail,
  )
  checkmaterial: Checkmaterial;

  @ManyToOne(() => Material, (material) => material.checkmaterialdetail)
  material: Material;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
