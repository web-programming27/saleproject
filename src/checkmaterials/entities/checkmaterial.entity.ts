import { User } from 'src/users/entities/user.entity';
import {
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  UpdateDateColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';
import { Checkmaterialdetail } from './checkmaterialdetail';

@Entity()
export class Checkmaterial {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(
    () => Checkmaterialdetail,
    (checkmaterialdetail) => checkmaterialdetail.checkmaterial,
  )
  checkmaterialdetail: Checkmaterialdetail[];

  @ManyToOne(() => User, (employee) => employee.checkmaterials)
  employee: User;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
