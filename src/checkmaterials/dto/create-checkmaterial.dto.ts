import { IsNotEmpty } from 'class-validator';

class CreateCheckmaterialdetailDto {
  @IsNotEmpty()
  materialId: number;
  @IsNotEmpty()
  qty_remain: number;
  @IsNotEmpty()
  qty_expire: number;
}
export class CreateCheckmaterialDto {
  @IsNotEmpty()
  employeeId: number;
  @IsNotEmpty()
  checkmaterialDetails: CreateCheckmaterialdetailDto[];
}
