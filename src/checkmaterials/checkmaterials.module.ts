import { Module } from '@nestjs/common';
import { CheckmaterialsService } from './checkmaterials.service';
import { CheckmaterialsController } from './checkmaterials.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkmaterial } from './entities/checkmaterial.entity';
import { User } from 'src/users/entities/user.entity';
import { Material } from 'src/materials/entities/material.entity';
import { Checkmaterialdetail } from './entities/checkmaterialdetail';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Checkmaterial,
      User,
      Material,
      Checkmaterialdetail,
    ]),
  ],
  controllers: [CheckmaterialsController],
  providers: [CheckmaterialsService],
})
export class CheckmaterialsModule {}
