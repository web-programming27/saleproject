import { Billdetail } from 'src/bills/entities/billdetail';
import { Checkmaterialdetail } from 'src/checkmaterials/entities/checkmaterialdetail';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  min_quantity: number;
  @Column()
  quantity: number;
  @Column()
  unit: string;
  @Column()
  price_per_unit: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => Billdetail, (billdetail) => billdetail.material)
  billdetails: Billdetail[];

  @OneToMany(
    () => Checkmaterialdetail,
    (checkmaterialdetail) => checkmaterialdetail.material,
  )
  checkmaterialdetail: Checkmaterialdetail[];
}
