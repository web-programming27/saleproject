import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateRecieptDto } from './dto/create-reciept.dto';
import { UpdateRecieptDto } from './dto/update-reciept.dto';
import { Reciept } from './entities/reciept.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Store } from 'src/stores/entities/store.entity';
import { Recieptdetail } from './entities/recieptdetail';
import { Product } from 'src/products/entities/product.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class RecieptsService {
  constructor(
    @InjectRepository(Reciept)
    private recieptsRepository: Repository<Reciept>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Store)
    private storesRepository: Repository<Store>,
    @InjectRepository(Recieptdetail)
    private recieptdetailsRepository: Repository<Recieptdetail>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(createRecieptDto: CreateRecieptDto) {
    console.log(createRecieptDto);
    const customer = await this.customersRepository.findOneBy({
      id: createRecieptDto.customerId,
    });
    const store = await this.storesRepository.findOneBy({
      id: createRecieptDto.storeId,
    });

    const employee = await this.usersRepository.findOneBy({
      id: createRecieptDto.employeeId,
    });
    console.log(customer, store, employee);
    const reciept = new Reciept();
    reciept.customer = customer;
    reciept.store = store;
    reciept.employee = employee;
    // reciept.queue = createRecieptDto.queue;
    reciept.received = createRecieptDto.received;
    reciept.discount = createRecieptDto.discount;
    reciept.payment = createRecieptDto.payment;
    reciept.amount = 0;
    reciept.total = 0;
    reciept.change = 0;
    await this.recieptsRepository.save(reciept);

    for (const rd of createRecieptDto.recieptDetails) {
      const recieptDetail = new Recieptdetail();
      recieptDetail.amount = rd.amount;
      recieptDetail.product = await this.productsRepository.findOneBy({
        id: rd.productId,
      });
      console.log(recieptDetail);
      recieptDetail.name = recieptDetail.product.name;
      recieptDetail.price = recieptDetail.product.price;
      recieptDetail.total = recieptDetail.price * recieptDetail.amount;
      recieptDetail.reciept = reciept;

      await this.recieptdetailsRepository.save(recieptDetail);
      reciept.amount = reciept.amount + recieptDetail.amount;
      reciept.total = reciept.total + recieptDetail.total;
      reciept.change = reciept.received - (reciept.total - reciept.discount);
      //payment ถ้า payment เป็น Card E-Wallet ให้ change = 0
      if (reciept.payment === 'Card') {
        reciept.change = 0;
      } else if (reciept.payment === 'E-Wallet') {
        reciept.change = 0;
      }
      console.log('change', reciept.change);
    }

    await this.recieptsRepository.save(reciept);
    return await this.recieptsRepository.findOne({
      where: { id: reciept.id },
      relations: ['recieptDetails'],
    });
  }

  findAll() {
    return this.recieptsRepository.find({
      relations: ['recieptDetails', 'customer', 'store', 'employee'],
    });
  }

  async findOne(id: number) {
    const reciept = await this.recieptsRepository.findOne({
      where: { id: id },
      relations: ['customer', 'store', 'employee', 'recieptDetails'],
    });
    if (!reciept) {
      throw new NotFoundException();
    }
    return reciept;
  }

  update(id: number, updateRecieptDto: UpdateRecieptDto) {
    return `This action updates a #${id} reciept`;
  }

  async remove(id: number) {
    const reciept = await this.recieptsRepository.findOneBy({ id: id });
    return this.recieptsRepository.softRemove(reciept);
  }
}
