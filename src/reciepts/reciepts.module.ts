import { Module } from '@nestjs/common';
import { RecieptsService } from './reciepts.service';
import { RecieptsController } from './reciepts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reciept } from './entities/reciept.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Store } from 'src/stores/entities/store.entity';
import { Recieptdetail } from './entities/recieptdetail';
import { Product } from 'src/products/entities/product.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Reciept,
      Recieptdetail,
      Customer,
      Store,
      User,
      Product,
    ]),
  ],
  controllers: [RecieptsController],
  providers: [RecieptsService],
})
export class RecieptsModule {}
