import { Product } from 'src/products/entities/product.entity';
import { Reciept } from 'src/reciepts/entities/reciept.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Recieptdetail {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  amount: number;
  @Column({ type: 'float' })
  price: number;
  @Column({ type: 'float' })
  total: number;

  @ManyToOne(() => Product, (product) => product.reciept)
  product: Product;

  @ManyToOne(() => Reciept, (reciept) => reciept.recieptDetails)
  reciept: Reciept;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
