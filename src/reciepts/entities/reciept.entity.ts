import { Customer } from 'src/customers/entities/customer.entity';
import { Store } from 'src/stores/entities/store.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Recieptdetail } from './recieptdetail';
import { User } from 'src/users/entities/user.entity';
@Entity()
export class Reciept {
  @PrimaryGeneratedColumn()
  id: number;
  // @Column()
  // queue: number;
  @CreateDateColumn()
  date: Date;
  @Column({ type: 'float' })
  discount: number;
  @Column({ type: 'float' })
  total: number;
  @Column({ type: 'float' })
  received: number;
  @Column({ type: 'float' })
  change: number;
  @Column()
  payment: string;
  @Column()
  amount: number;

  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => Recieptdetail, (recieptDetails) => recieptDetails.reciept)
  recieptDetails: Recieptdetail[];

  @ManyToOne(() => Customer, (customer) => customer.reciepts)
  customer: Customer;

  @ManyToOne(() => Store, (store) => store.reciepts)
  store: Store;

  @ManyToOne(() => User, (employee) => employee.reciepts)
  employee: User;
}
