import { IsNotEmpty, IsPositive } from 'class-validator';
class CreateRecieptDetailDto {
  @IsNotEmpty()
  productId: number;
  @IsNotEmpty()
  // @IsPositive()
  amount: number;
}

export class CreateRecieptDto {
  @IsNotEmpty()
  customerId: number;
  @IsNotEmpty()
  storeId: number;
  @IsNotEmpty()
  employeeId: number;
  // @Generated('increment')
  // @IsPositive()
  // queue: number;
  @IsNotEmpty()
  recieptDetails: CreateRecieptDetailDto[];
  @IsNotEmpty()
  // @IsPositive()
  received: number;
  @IsNotEmpty()
  // @IsPositive()
  discount: number;
  @IsNotEmpty()
  payment: string;
}
